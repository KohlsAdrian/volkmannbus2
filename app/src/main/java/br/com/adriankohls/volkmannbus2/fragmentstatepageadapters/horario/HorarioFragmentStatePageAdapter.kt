package br.com.adriankohls.volkmannbus2.fragmentstatepageadapters.horario

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.fragments.horario.HorarioShowFragment

/**
 * Created by adriankohls on 24/10/17.
 */

class HorarioFragmentStatePageAdapter(fm: FragmentManager, private val context: Context, private val isToBlumenau: Boolean) : FragmentStatePagerAdapter(fm) {


    override fun getPageTitle(position: Int): CharSequence {
        return context.resources.getStringArray(R.array.horario_semana)[position]
    }

    override fun getItem(position: Int): Fragment {
        return HorarioShowFragment.newInstance(isToBlumenau, position)
    }

    override fun getCount(): Int {
        return 3
    }
}
