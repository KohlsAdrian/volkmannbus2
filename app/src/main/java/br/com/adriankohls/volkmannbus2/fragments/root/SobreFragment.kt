package br.com.adriankohls.volkmannbus2.fragments.root

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.TextView
import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.utils.AppUtils
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

/**
 * Created by adriankohls on 12/02/18.
 */

class SobreFragment : Fragment() {

    private lateinit var tvDateUpdate : TextView
    private lateinit var cardEmail : CardView
    private lateinit var donateView : WebView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_sobre, container, false)
        tvDateUpdate = view.findViewById(R.id.tv_date_update)
        cardEmail = view.findViewById(R.id.cardEmail)
        donateView = view.findViewById(R.id.webviewdonate)
        firebaseGetDateUpdate()
        setActions()
        return view
    }

    private fun setActions() {
        cardEmail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "adriankohls95@gmail.com", null))
            intent.putExtra(Intent.EXTRA_EMAIL, Array(1, init = {"adriankohls95@gmail.com"}))
            startActivity(Intent.createChooser(intent, "Enviar e-mail"))
        }
        payPalDonate()
    }

    private fun firebaseGetDateUpdate() {
        val sp : SharedPreferences = activity!!.getSharedPreferences("firebase_database", Context.MODE_PRIVATE)
        var date = sp.getString("date", "--/--/----")
        tvDateUpdate.text = date
        val db = FirebaseDatabase.getInstance()
        val dbref = db.reference.child("dataAtualizacao")
        dbref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot?) {
                date = p0!!.value as String
                tvDateUpdate.text = date
                sp.edit().putString("date", date).commit()
            }

            override fun onCancelled(p0: DatabaseError?) {

            }

        })
    }

    private fun payPalDonate(){
        val html = "<form style=\"text-align: center; background-color: rgba(0,0,0,.5);\" action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" target=\"_top\">\n" +
                "<input type=\"hidden\" name=\"cmd\" value=\"_s-xclick\">\n" +
                "<input type=\"hidden\" name=\"encrypted\" value=\"-----BEGIN PKCS7-----MIIHXwYJKoZIhvcNAQcEoIIHUDCCB0wCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYA5GCuUOMTefiyxYYK4+qR20Zd2DnmcO+xaRZT0vODn24JBvTEEk7B3KNf9P668cMFtCwV3szryCmta42NrAU/dbzGSscXJ9gS75R75wTkO8MNgOlVLLT695+ScFgJAkbshLxWIknV0VPFJJVL1OtMbAIWFEAsz5P0uvByqslgqaDELMAkGBSsOAwIaBQAwgdwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIYxnt+2GsZZOAgbicZfbF3u2BWJQ2R5Gyw7RubgsQJqlAZ/2G3vA+eq9afb2IPxcxtskVKCe+ieHJi3nQK6R+K+6T+UFFcI4d056M6KfwtpFM7wZ3p+v3X7PtQqIPB1bfrW5UYe+hLcXJJ39rOjWVJh+kLlnEj2NMfaUGzqwCtJFBsiiAN0qjr1q09nj7IzsFq3ASe2KfPfHXp/jXQOhS6slsSfaxCRUizpKZb5gYm2FJApEd9cKq+HKzpmMWVjL5XAZFoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTgwMjEyMTI0ODI1WjAjBgkqhkiG9w0BCQQxFgQU62zcZ79uOwI7bWvdVvnbFJuSjCkwDQYJKoZIhvcNAQEBBQAEgYC0mxa/QjuDLgUgJZTtPzKp8oTEhkh62V4YhaotOb/zhMXXXyyHB7AVnSEHTTefqHIn51j0SjnUbfEKl3Gxg0Z6TRGjORfheq/xsbuqoG/sfwOWZXmhPB6i0MSw5uFw05oIQE02vbMdLash6GZi+S+zq2sp6Ybdb1HKZdaneA3LeQ==-----END PKCS7-----\n" +
                "\">\n" +
                "<input type=\"image\" src=\"https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif\" border=\"0\" name=\"submit\" alt=\"PayPal - A maneira fácil e segura de enviar pagamentos online!\">\n" +
                "<img alt=\"\" border=\"0\" src=\"https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif\" width=\"1\" height=\"1\">\n" +
                "</form>\n"

        donateView.settings.javaScriptEnabled = true
        donateView.loadData(html, "text/html", null)
    }

    companion object {
        fun newInstance() : SobreFragment {
            val bundle = Bundle()
            val fragment = SobreFragment()
            bundle.putString(fragment.tag, AppUtils.getFragmentKey(fragment))
            fragment.arguments = bundle
            return fragment
        }
    }

}