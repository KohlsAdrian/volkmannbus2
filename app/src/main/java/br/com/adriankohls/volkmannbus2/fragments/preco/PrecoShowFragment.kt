package br.com.adriankohls.volkmannbus2.fragments.preco

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.recyclerviewadapters.preco.PrecoRecyclerViewAdapter
import br.com.adriankohls.volkmannbus2.utils.AppUtils

/**
 * Created by Kohls647 on 30/12/2015.
 */
class PrecoShowFragment : Fragment() {

    private val precos: Array<String>? = null
    private var index: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.recyclerview, container, false)

        val recyclerView = rootView.findViewById<RecyclerView>(R.id.recyclerview)
        recyclerView.setHasFixedSize(false)
        recyclerView.layoutManager = LinearLayoutManager(activity)

        val adapter = PrecoRecyclerViewAdapter(activity!!, index)

        recyclerView.adapter = adapter
        return rootView
    }

    companion object {

        fun newInstance(index: Int): PrecoShowFragment {
            val bundle = Bundle()
            val fragment = PrecoShowFragment()
            fragment.index = index
            bundle.putString(fragment.tag, AppUtils.getFragmentKey(fragment))
            bundle.putInt("index", index)
            fragment.arguments = bundle
            return fragment
        }
    }

}
