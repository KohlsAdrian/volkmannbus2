package br.com.adriankohls.volkmannbus2

import android.app.Application
import com.google.firebase.FirebaseApp

/**
 * Created by adriankohls on 27/10/17.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
    }
}