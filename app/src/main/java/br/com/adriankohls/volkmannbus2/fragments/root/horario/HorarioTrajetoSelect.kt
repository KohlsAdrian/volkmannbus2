package br.com.adriankohls.volkmannbus2.fragments.root.horario

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton

import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.fragments.root.PrincipalFragment
import br.com.adriankohls.volkmannbus2.utils.AppUtils

/**
 * Created by Kohls on 8/5/2016.
 */
class HorarioTrajetoSelect : Fragment() {

    private var f: PrincipalFragment? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.recyclerview_horario_select, container, false)

        view.findViewById<View>(R.id.btPomBnu).setOnClickListener {
            AppUtils.fragmentTransaction(HorarioFragment.newInstance(true), activity!!, R.id.corpo_fragment_main)
        }

        view.findViewById<View>(R.id.btBnuPom).setOnClickListener {
            AppUtils.fragmentTransaction(HorarioFragment.newInstance(false), activity!!, R.id.corpo_fragment_main)
        }

        return view
    }

    companion object {

        fun newInstance(f: PrincipalFragment): HorarioTrajetoSelect {
            val bundle = Bundle()
            val fragment = HorarioTrajetoSelect()
            fragment.f = f
            bundle.putString(fragment.tag, AppUtils.getFragmentKey(fragment))
            fragment.arguments = bundle
            return fragment
        }
    }
}
