package br.com.adriankohls.volkmannbus2.fragmentstatepageadapters.preco

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.fragments.preco.PrecoShowFragment

/**
 * Created by Kohls647 on 30/12/2015.
 */
class PrecoFragmentStatePageAdapter(fm: FragmentManager, private val context: Context) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return PrecoShowFragment.newInstance(position)
            1 -> return PrecoShowFragment.newInstance(position)
            2 -> return PrecoShowFragment.newInstance(position)
            else -> return null
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        return context.resources.getStringArray(R.array.preco_nomes)[position]
    }

    override fun getCount(): Int {
        return context.resources.getStringArray(R.array.preco_nomes).size
    }
}
