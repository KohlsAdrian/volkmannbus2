package br.com.adriankohls.volkmannbus2.fragments.root

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.fragments.root.horario.HorarioTrajetoSelect
import br.com.adriankohls.volkmannbus2.utils.AppUtils
import br.com.adriankohls.volkmannbus2.utils.BottomNavigationViewHelper

/**
 * Created by Kohls647 on 22/01/2016.
 */
class PrincipalFragment : Fragment() {

    private lateinit var bottomNavigation : BottomNavigationView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_frame_main, container, false)
        AppUtils.fragmentTransactionFirstFragment(HorarioTrajetoSelect.newInstance(this), activity!!, R.id.corpo_fragment_main).commit()

        bottomNavigation = view.findViewById(R.id.nav_main)
        BottomNavigationViewHelper.disableShiftMode(bottomNavigation)

        (view!!.findViewById<View>(R.id.nav_main) as BottomNavigationView).setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { item ->
            var f: Fragment? = null
            when (item.itemId) {
                R.id.main_horario -> f = HorarioTrajetoSelect.newInstance(this@PrincipalFragment)
                R.id.main_preco -> f = PrecoFragment.newInstance()
                R.id.main_itinerario -> f = ItinerarioFragment.newInstance()
                R.id.main_sobre -> f = SobreFragment.newInstance()
            }
            AppUtils.fragmentTransactionFirstFragment(f!!, activity!!, R.id.corpo_fragment_main).commit()
            return@OnNavigationItemSelectedListener true
        })
        return view
    }

    companion object {

        fun newInstance(): PrincipalFragment {
            val bundle = Bundle()
            val fragment = PrincipalFragment()
            bundle.putString(fragment.tag, AppUtils.getFragmentKey(fragment))
            fragment.arguments = bundle
            return fragment
        }
    }

}
