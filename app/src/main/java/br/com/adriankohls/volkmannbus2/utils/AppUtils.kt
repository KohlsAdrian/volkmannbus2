package br.com.adriankohls.volkmannbus2.utils

import android.content.Context
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import br.com.adriankohls.volkmannbus2.R


/**
 * Created by Kohls647 on 31/12/2015.
 */
object AppUtils {

    fun isTablet(context: Context): Boolean {
        return context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_LARGE
    }

    fun tabLayoutRuleTablet(tabLayout: TabLayout, context: Context) {
        if (AppUtils.isTablet(context)) {
            tabLayout.tabGravity = TabLayout.GRAVITY_FILL
            tabLayout.tabMode = TabLayout.MODE_FIXED
        }
    }

    fun fragmentTransaction(fragment: Fragment, context: Context, idCorpoFragment: Int) {
        val appact = context as AppCompatActivity
        fragmentTransactionFirstFragment(fragment, appact, idCorpoFragment).addToBackStack(fragment.javaClass.name).commit()
    }

    fun fragmentTransactionFirstFragment(fragment: Fragment, context: FragmentActivity, idCorpoFragment: Int): FragmentTransaction {
        val ft = context.supportFragmentManager.beginTransaction()
        ft.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out, R.anim.abc_fade_in, R.anim.abc_fade_out)
        ft.replace(idCorpoFragment, fragment)
        return ft
    }


    fun getFragmentKey(fragment: Fragment): String {
        val sb = StringBuilder()
        val a = fragment.hashCode()
        val b = fragment.javaClass.name
        return sb.append(a).append(b).toString()
    }

    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }
}
