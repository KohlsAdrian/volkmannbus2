package br.com.adriankohls.volkmannbus2.recyclerviewadapters.itinerario

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

import br.com.adriankohls.volkmannbus2.R

/**
 * Created by Kohls647 on 17/05/2016.
 */
class ItinerarioRecyclerViewAdapter(private val resourceRota: Array<String>) : RecyclerView.Adapter<ItinerarioRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItinerarioRecyclerViewAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_itinerario, parent, false))
    }

    override fun onBindViewHolder(holder: ItinerarioRecyclerViewAdapter.ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return resourceRota.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val layout: LinearLayout = itemView.findViewById(R.id.layout_item_text)
        private val tvLarge: TextView = itemView.findViewById(R.id.tvLarge)

        fun bind(position: Int) {
            if (position % 2 == 0)
                layout.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorPrimaryTransp))
            else
                layout.setBackgroundColor(ContextCompat.getColor(itemView.context, android.R.color.white))
            tvLarge.text = resourceRota[position]
        }
    }
}
