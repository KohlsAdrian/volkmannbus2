package br.com.adriankohls.volkmannbus2.fragments.horario

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.recyclerviewadapters.horario.HorarioRecyclerViewAdapter

/**
 * Created by adriankohls on 24/10/17.
 */

class HorarioShowFragment : Fragment() {

    private var isToBlumenau: Boolean = false
    private var diaSemana: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.layout_horario, container, false)
        val rvHorario = view!!.findViewById<RecyclerView>(R.id.rv_horario)
        rvHorario.layoutManager = LinearLayoutManager(activity)
        rvHorario.adapter = HorarioRecyclerViewAdapter(activity!!, isToBlumenau, diaSemana)
        return view
    }

    companion object {

        fun newInstance(isToBlumenau: Boolean, diaSemana: Int): HorarioShowFragment {

            val args = Bundle()

            val fragment = HorarioShowFragment()
            fragment.isToBlumenau = isToBlumenau
            fragment.diaSemana = diaSemana
            fragment.arguments = args
            return fragment
        }
    }
}
