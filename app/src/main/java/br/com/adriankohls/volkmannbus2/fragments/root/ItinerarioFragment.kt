package br.com.adriankohls.volkmannbus2.fragments.root

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.fragmentstatepageadapters.itinerario.ItinerarioFragmentStatePageAdapter
import br.com.adriankohls.volkmannbus2.utils.AppUtils

/**
 * Created by Kohls647 on 09/01/2016.
 */
class ItinerarioFragment : Fragment() {
    private var rootView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_viewpager_tablayout, container, false)
            val viewPager = rootView!!.findViewById<ViewPager>(R.id.viewpager)
            val tabLayout = rootView!!.findViewById<TabLayout>(R.id.tabs)
            viewPager.adapter = ItinerarioFragmentStatePageAdapter(childFragmentManager, activity!!)
            tabLayout.setupWithViewPager(viewPager)
        }
        return rootView
    }

    companion object {

        fun newInstance(): ItinerarioFragment {
            val bundle = Bundle()
            val fragment = ItinerarioFragment()
            bundle.putString(fragment.tag, AppUtils.getFragmentKey(fragment))
            fragment.arguments = bundle
            return fragment
        }
    }
}
