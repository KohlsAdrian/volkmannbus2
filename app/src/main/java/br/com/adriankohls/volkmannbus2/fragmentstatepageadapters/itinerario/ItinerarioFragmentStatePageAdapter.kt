package br.com.adriankohls.volkmannbus2.fragmentstatepageadapters.itinerario

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.fragments.itinerario.ItinerarioShowFragment
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Kohls647 on 09/01/2016.
 */
class ItinerarioFragmentStatePageAdapter(fm: FragmentManager, private val context: Context) : FragmentStatePagerAdapter(fm) {
    private val rota: Array<String> = context.resources.getStringArray(R.array.rota)

    override fun getPageTitle(position: Int): CharSequence {
        return rota[position]
    }

    override fun getItem(position: Int): Fragment? {
        val sp = context.getSharedPreferences("firebase_database", Context.MODE_PRIVATE)
        if(sp.getString("itinerario", "").isNotEmpty()){
            val cidade = JSONObject(sp.getString("itinerario", ""))
            lateinit var itinerario: JSONArray
            when(position){
                0 -> itinerario = cidade.getJSONArray("pombnu")
                1 -> itinerario = cidade.getJSONArray("bnupom")
            }
            return ItinerarioShowFragment.newInstance(
                    Gson().fromJson(itinerario.toString(), Array(itinerario.length(), init = {""}).javaClass),
                    position)
        }
        return when (position) {
            0 -> ItinerarioShowFragment.newInstance(context.resources.getStringArray(R.array.itinerario_pomerodeblumenau), position)
            1 -> ItinerarioShowFragment.newInstance(context.resources.getStringArray(R.array.itinerario_blumenaupomerode), position)
            else -> null
        }
    }

    override fun getCount(): Int {
        return rota.size
    }
}
