package br.com.adriankohls.volkmannbus2.main

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.fragments.root.PrincipalFragment
import br.com.adriankohls.volkmannbus2.utils.AppUtils
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sp : SharedPreferences = getSharedPreferences("firebase_database", Context.MODE_PRIVATE)

        val update : Long = sp.getLong("update",0)

        AppUtils.fragmentTransactionFirstFragment(PrincipalFragment.newInstance(), this, R.id.corpo_fragment).commit()

        AlertDialog.Builder(this)
                .setTitle(getString(R.string.aviso_seguranca))
                .setMessage(getString(R.string.seguranca_message))
                .show()


        if(AppUtils.isOnline(this)){
            val db = FirebaseDatabase.getInstance()
            val dbref = db.reference.child("atualizacao")

            dbref.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {
                    if(p0?.value as Long > update){
                        val diagUpdate = AlertDialog.Builder(this@MainActivity)
                        diagUpdate.setTitle(getString(R.string.question_update))
                        diagUpdate.setMessage(getString(R.string.message_update))
                        diagUpdate.setPositiveButton(getString(R.string.sim)) { _, _ ->
                            val fs = FirebaseStorage.getInstance()
                            val fsrH = fs.reference.child("horarios.json")
                            val horarioF = File.createTempFile("horario", "json")
                            fsrH.getFile(horarioF).addOnSuccessListener {
                                val input = horarioF.inputStream()
                                val size = input.available()
                                val buffer = ByteArray(size)
                                input.read(buffer)
                                input.close()
                                val json = String(buffer, charset("UTF-8"))
                                sp.edit().putString("horarios", json).commit()
                                Log.d("horarios", sp.getString("horarios",""))
                            }
                            val fsrP = fs.reference.child("preco.json")
                            val precoF = File.createTempFile("preco", "json")
                            fsrP.getFile(precoF).addOnSuccessListener {
                                val input = precoF.inputStream()
                                val size = input.available()
                                val buffer = ByteArray(size)
                                input.read(buffer)
                                input.close()
                                val json = String(buffer, charset("UTF-8"))
                                sp.edit().putString("preco", json).commit()
                                Log.d("preco", sp.getString("preco",""))
                            }
                            val fsrI = fs.reference.child("itinerario.json")
                            val itinerarioF = File.createTempFile("itinerario", "json")
                            fsrI.getFile(itinerarioF).addOnSuccessListener {
                                val input = itinerarioF.inputStream()
                                val size = input.available()
                                val buffer = ByteArray(size)
                                input.read(buffer)
                                input.close()
                                val json = String(buffer, charset("UTF-8"))
                                sp.edit().putString("itinerario", json).commit()
                                Log.d("itinerario", sp.getString("itinerario",""))
                            }
                            sp.edit().putLong("update", p0.value as Long).commit()

                            Toast.makeText(this@MainActivity,
                                    getString(R.string.atualizacao_ok),
                                    Toast.LENGTH_LONG).show()
                        }
                        diagUpdate.setNegativeButton(getString(R.string.nao), null)
                        diagUpdate.show()
                    }
                }
            })
        }

    }

}
