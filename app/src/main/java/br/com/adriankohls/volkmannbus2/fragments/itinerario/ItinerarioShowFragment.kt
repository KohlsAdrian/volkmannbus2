package br.com.adriankohls.volkmannbus2.fragments.itinerario

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.recyclerviewadapters.itinerario.ItinerarioRecyclerViewAdapter
import br.com.adriankohls.volkmannbus2.utils.AppUtils

/**
 * Created by Kohls647 on 09/01/2016.
 */
class ItinerarioShowFragment : Fragment() {
    private var rootView: View? = null

    private var resource: Array<String>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.recyclerview, container, false)
            val recyclerView = rootView!!.findViewById<RecyclerView>(R.id.recyclerview)
            recyclerView.setHasFixedSize(false)
            recyclerView.layoutManager = LinearLayoutManager(activity!!)
            recyclerView.adapter = ItinerarioRecyclerViewAdapter(resource!!)
        }
        return rootView
    }

    companion object {

        fun newInstance(resource: Array<String>, index: Int): ItinerarioShowFragment {
            val bundle = Bundle()
            val fragment = ItinerarioShowFragment()
            fragment.resource = resource
            bundle.putString(fragment.tag, AppUtils.getFragmentKey(fragment))
            bundle.putInt("index", index)
            fragment.arguments = bundle
            return fragment
        }
    }
}
