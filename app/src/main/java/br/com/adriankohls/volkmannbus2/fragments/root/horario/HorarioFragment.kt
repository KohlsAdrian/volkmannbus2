package br.com.adriankohls.volkmannbus2.fragments.root.horario

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import java.util.Calendar

import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.fragmentstatepageadapters.horario.HorarioFragmentStatePageAdapter
import br.com.adriankohls.volkmannbus2.utils.AppUtils

/**
 * Created by Kohls647 on 09/01/2016.
 */
class HorarioFragment : Fragment() {

    private var isToBlumenau: Boolean = false
    private var rootView: View? = null
    private var tvTrajeto: TextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_viewpager_tablayout_horario, container, false)
            val viewPager = rootView!!.findViewById<ViewPager>(R.id.viewpager)
            tvTrajeto = rootView!!.findViewById(R.id.tvTrajeto)
            if (isToBlumenau) {
                tvTrajeto!!.text = resources.getString(R.string.pomerodeblumenau)
            } else {
                tvTrajeto!!.text = resources.getString(R.string.blumenaupomerode)
            }
            viewPager.adapter = HorarioFragmentStatePageAdapter(childFragmentManager, activity!!, isToBlumenau)
            val tabLayout = rootView!!.findViewById<TabLayout>(R.id.tabs)
            tabLayout.setupWithViewPager(viewPager)
            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }

                override fun onPageSelected(position: Int) {

                }

                override fun onPageScrollStateChanged(state: Int) {
                    (activity!!.findViewById<View>(R.id.appbar) as AppBarLayout).setExpanded(true)
                }
            })
            when (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
                Calendar.SUNDAY -> viewPager.setCurrentItem(2, true)
                Calendar.SATURDAY -> viewPager.setCurrentItem(1, true)
                else -> viewPager.setCurrentItem(0, true)
            }
            AppUtils.tabLayoutRuleTablet(tabLayout, activity!!)
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        tvTrajeto!!.setOnClickListener {
            if (isToBlumenau) {
                AppUtils.fragmentTransactionFirstFragment(HorarioFragment.newInstance(false), activity!!, R.id.corpo_fragment_main).commit()
            } else {
                AppUtils.fragmentTransactionFirstFragment(HorarioFragment.newInstance(true), activity!!, R.id.corpo_fragment_main).commit()
            }
        }
    }

    companion object {

        fun newInstance(isToBlumenau: Boolean): HorarioFragment {
            val bundle = Bundle()
            val fragment = HorarioFragment()
            fragment.isToBlumenau = isToBlumenau
            bundle.putString(fragment.tag, AppUtils.getFragmentKey(fragment))
            fragment.arguments = bundle
            return fragment
        }
    }
}
