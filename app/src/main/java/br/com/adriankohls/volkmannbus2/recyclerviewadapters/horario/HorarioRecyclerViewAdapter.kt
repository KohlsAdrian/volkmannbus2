package br.com.adriankohls.volkmannbus2.recyclerviewadapters.horario

import android.content.Context
import android.content.SharedPreferences
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import br.com.adriankohls.volkmannbus2.R
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by adriankohls on 19/10/17.
 */

class HorarioRecyclerViewAdapter(context: Context, private val isToBlumenau: Boolean, private val diaSemana: Int) : RecyclerView.Adapter<HorarioRecyclerViewAdapter.VH>() {

    private lateinit var hora: Array<String>

    //Firebase Downloaded Files
    private var newHoras = false
    private lateinit var newHora: JSONArray

    init {
        val sp: SharedPreferences = context.getSharedPreferences("firebase_database", Context.MODE_PRIVATE)

        if (sp.getString("horarios", "").isEmpty()) {
            if (isToBlumenau) {
                when (diaSemana) {
                    0 -> hora = context.resources.getStringArray(R.array.pombnu_segsex)
                    1 -> hora = context.resources.getStringArray(R.array.pombnu_sab)
                    2 -> hora = context.resources.getStringArray(R.array.pombnu_dom)
                }
            } else {
                when (diaSemana) {
                    0 -> hora = context.resources.getStringArray(R.array.bnupom_segsex)
                    1 -> hora = context.resources.getStringArray(R.array.bnupom_sab)
                    2 -> hora = context.resources.getStringArray(R.array.bnupom_dom)
                }
            }
        } else {
            newHoras = true
            val horarios = JSONObject(sp.getString("horarios", ""))

            val cidade = if (isToBlumenau) {
                horarios.getJSONObject("pombnu")
            } else {
                horarios.getJSONObject("bnupom")
            }
            when (diaSemana) {
                0 -> newHora = cidade.getJSONArray("diasuteis")
                1 -> newHora = cidade.getJSONArray("sabado")
                2 -> newHora = cidade.getJSONArray("domfer")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HorarioRecyclerViewAdapter.VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.item_horario, parent, false))
    }

    override fun onBindViewHolder(holder: HorarioRecyclerViewAdapter.VH, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        if(newHoras)
            return newHora.length()
        return hora.size
    }

    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var tvHorario: TextView = itemView.findViewById(R.id.tvHorario)
        private var tvDescricao: TextView = itemView.findViewById(R.id.tvDescricao)
        internal var layout: LinearLayout = itemView.findViewById(R.id.layout_item_horario)

        init {
            tvDescricao.visibility = View.GONE
        }

        fun bind(position: Int) {
            if (position % 2 == 0)
                layout.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorPrimaryTransp))
            else
                layout.setBackgroundColor(ContextCompat.getColor(itemView.context, android.R.color.white))

            if (newHoras) {
                if (newHora[position] is String)
                    tvHorario.text = newHora[position] as String
                else {
                    tvHorario.text = newHora.getJSONObject(position).getString("hora")
                    tvDescricao.text = newHora.getJSONObject(position).getString("legenda")
                    tvDescricao.visibility = View.VISIBLE
                }
            } else {

                tvHorario.text = hora[position]

                if (diaSemana == 0) {
                    var desc: String? = null
                    if (isToBlumenau) {
                        when (position) {
                            3 -> desc = itemView.resources.getString(R.string.segsex_pomerodeblumenau_06_30)
                            5 -> desc = itemView.resources.getString(R.string.segsex_pomerodeblumenau_07_30)
                            17 -> desc = itemView.resources.getString(R.string.segsex_pomerodeblumenau_17_15)
                            22 -> desc = itemView.resources.getString(R.string.segsex_pomerodeblumenau_22_10)
                        }
                    } else {
                        when (position) {
                            0 -> desc = itemView.resources.getString(R.string.segsex_blumenaupomerode_03_55)
                            2 -> desc = itemView.resources.getString(R.string.segsex_blumenaupomerode_06_15)
                            3 -> desc = itemView.resources.getString(R.string.segsex_blumenaupomerode_06_50)
                        }
                    }
                    if (desc != null) {
                        tvDescricao.visibility = View.VISIBLE
                        tvDescricao.text = desc
                    }
                }
            }
        }
    }
}
