package br.com.adriankohls.volkmannbus2.recyclerviewadapters.preco

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import br.com.adriankohls.volkmannbus2.R
import br.com.adriankohls.volkmannbus2.utils.AppUtils
import com.google.gson.Gson

/**
 * Created by Kohls647 on 26/04/2016.
 */
class PrecoRecyclerViewAdapter(context: Context, indexPasse: Int) : RecyclerView.Adapter<PrecoRecyclerViewAdapter.ViewHolder>() {

    private val titulo: Array<String> = context.resources.getStringArray(R.array.preco_frota)
    private var conteudo: Array<String> = context.resources.getStringArray(R.array.preco_normal)
    private val moeda = "R$ "

    init {
        val sp = context.getSharedPreferences("firebase_database", Context.MODE_PRIVATE)
        if (sp.getString("preco", "").isNotEmpty()) {
            val preco = sp.getString("preco", "")
            conteudo = Gson().fromJson(preco, Array(titulo.size, init = { "" }).javaClass)
        }

        when (indexPasse) {
            0 -> for (i in conteudo.indices) {
                conteudo[i] = conteudo[i].replace(".", ",")
            }
            1 -> for (i in conteudo.indices) {
                val valor = java.lang.Double.parseDouble(conteudo[i])
                conteudo[i] = String.format("%.2f", valor * 50) //trabalhador
                conteudo[i] = conteudo[i].replace(".", ",")
            }
            2 -> for (i in conteudo.indices) {
                val valor = java.lang.Double.parseDouble(conteudo[i])
                conteudo[i] = String.format("%.2f", valor * 50 / 2) //estudante
                conteudo[i] = conteudo[i].replace(".", ",")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_passagem, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return titulo.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvRota: TextView = itemView.findViewById(R.id.tvRota)
        private val tvPreco: TextView = itemView.findViewById(R.id.tvPassagem)
        private val layoutPrice: LinearLayout = itemView.findViewById(R.id.layoutPrice)

        fun bind(position: Int) {
            tvRota.text = titulo[position]
            tvPreco.text = moeda + conteudo[position]

            if (position % 2 == 0)
                layoutPrice.setBackgroundColor(ContextCompat.getColor(itemView.context, android.R.color.white))
            else
                layoutPrice.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorPrimaryTransp))
            if (AppUtils.isTablet(itemView.context)) {
                tvRota.textSize = 20f
                tvPreco.textSize = 18f
            }
        }
    }

}
